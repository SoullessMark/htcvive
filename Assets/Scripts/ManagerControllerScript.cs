﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerControllerScript : MonoBehaviour {

    public static ManagerControllerScript Get;

    public GameObject leftController;
    public GameObject rightController;

    private bool isDown = false;

    private Ambit ambit = new Ambit();

    void Awake()
    { Get = this;
    }

    public void ProcessTrigger()
    {
        float distance = ambit.Get(leftController.gameObject.transform.position.x, rightController.gameObject.transform.position.x);

        distance *= .004f;

        if (distance < .005f) { distance = .005f; }
        if (distance > .02f) { distance = .02f; }

        Vector3 scale = new Vector3(distance, distance, distance);

        print(scale);

        ManagerModelScript.Get.model.transform.localScale = scale;

    }

    public void ProcessTriggerDown()
    {
        if (!isDown)
        {
            
            ambit.Orgin(leftController.gameObject.transform.position.x, rightController.gameObject.transform.position.x);
            isDown = true;
        }
    }

    public void ProcessTriggerUp()
    {
        isDown = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
