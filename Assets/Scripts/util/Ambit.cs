﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ambit : System.Object {

    public Vector2 Constraint;

    private Vector2 orgin = new Vector2();
    private float orginDistance;

    private Vector2 current = new Vector2();
    private float currentDistance;

    private float center;
    private float step;

    public void Orgin ( float pos_controllerLeft, float pos_controllerRight)
    {

        orgin.x = pos_controllerLeft;
        orgin.y = pos_controllerRight;

        orginDistance = orgin.y - orgin.x;

        /*
        orgin.x = pos_controllerLeft;
        orgin.y = pos_controllerRight;

        center = ((pos_controllerRight - pos_controllerLeft) * .5f); //find the world cord. between two controllers and half the distance to find middle
        step = ((pos_controllerRight - pos_controllerLeft) * .01f); //same but get how big a 1/100 slice of "space" between the two

        Debug.Log("POS: " + orgin.x + " _ " + orgin.y + " Center : " + center + " Step size : " + step); */

    }

    public float Get (float pos_controllerLeft, float pos_controllerRight)
    {


        current.x = pos_controllerLeft;
        current.y = pos_controllerRight;

        currentDistance = current.y - current.x;

        return currentDistance / orginDistance;

        /*
        float working = 0;
        Vector2 diffPos = new Vector2(pos_controllerLeft - orgin.x, pos_controllerRight - orgin.y);

        Vector2 diffStep = new Vector2((diffPos.x * .01f), (diffPos.y * .01f));

        float diffWorld = diffStep.y - diffStep.x;

        Debug.Log(diffWorld);

        return diffWorld; */

    }


}
