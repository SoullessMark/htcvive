﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scalerScript : MonoBehaviour {

    public Transform WorldTransform;

    public float Scale;

    public Vector3 ScaleToBe;

    void Start()
    {
        
        ScaleToBe = WorldTransform.transform.localScale;
        Scale = ScaleToBe.x;
    } 

    void Update()
    {

        ScaleToBe.x = Scale;
        ScaleToBe.y = Scale;
        ScaleToBe.z = Scale;

        WorldTransform.transform.localScale = ScaleToBe;

    }


 
	
	
}
